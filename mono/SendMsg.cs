﻿using System;

using Newtonsoft.Json;

abstract class SendMsg
{
    public virtual string ToJson()
    {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
    }
    protected virtual Object MsgData()
    {
        return this;
    }

    protected abstract string MsgType();
}

abstract class SendMsgEx: SendMsg
{
    public override string ToJson()
    {
        return JsonConvert.SerializeObject(new MsgWrapperEx(this.MsgType(), this.MsgData(), this.GameTick()));
    }

    protected abstract int GameTick();
}

class Join : SendMsg
{
    public string name;
    public string key;

    public Join(string name, string key)
    {
        this.name = name;
        this.key = key;
    }

    protected override string MsgType()
    {
        return "join";
    }
}

class JoinRace : SendMsg
{
    public BotId botId;
    public string trackName;
    public int carCount;

    protected override string MsgType()
    {
        return "joinRace";
    }

    public JoinRace(string name, string key, string trackName = "keimola", int carCount = 1)
    {
        botId = new BotId(name, key);
        this.trackName = trackName;
        this.carCount = carCount;
    }
}

class Ping : SendMsg
{
    protected override string MsgType()
    {
        return "ping";
    }
}

class Turbo : SendMsg
{
    protected override string MsgType()
    {
        return "turbo";
    }

    protected override object MsgData()
    {
        return "GO!!!";
    }
}

class Throttle : SendMsgEx
{
    public double value;
    public int gameTick;

    public Throttle(double value, int gameTick)
    {
        this.value = value;
        this.gameTick = gameTick;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "throttle";
    }

    protected override int GameTick()
    {
        return this.gameTick;
    }
}

public enum Direction
{
    None,
    Right,
    Left
}

class SwitchLane : SendMsg
{
    private string data;

    protected override string MsgType()
    {
        return "switchLane";
    }

    public SwitchLane(Direction direction)
    {
        data = direction.ToString();
    }

    protected override object MsgData()
    {
        //Console.WriteLine(data);
        return data;
    }
}
