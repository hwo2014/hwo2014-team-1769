﻿using System;

public class MsgWrapper
{
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }
}

public class MsgWrapperEx : MsgWrapper
{
    public int gameTick;

    public MsgWrapperEx(string msgType, Object data, int gameTick)
        : base(msgType, data)
    {
        this.gameTick = gameTick;
    }
}

class BotId
{
    public string name;
    public string key;

    public BotId(string name, string key)
    {
        this.name = name;
        this.key = key;
    }
}

public class CarPositionData
{
    public Id id;
    public double angle;
    public PiecePosition piecePosition;

    public CarPositionData(Id id, double angle, PiecePosition piecePosition)
    {
        this.id = id;
        this.angle = angle;
        this.piecePosition = piecePosition;
    }
}

public class Id
{
    public string name;
    public string color;

    public Id(string name, string color)
    {
        this.name = name;
        this.color = color;
    }
}

public class PiecePosition
{
    public int pieceIndex;
    public double inPieceDistance;
    public Lane lane;
    public int lap;

    public PiecePosition(int pieceIndex, double inPieceDistance, Lane lane, int lap)
    {
        this.pieceIndex = pieceIndex;
        this.inPieceDistance = inPieceDistance;
        this.lane = lane;
        this.lap = lap;
    }
}

public class Lane
{
    public int startLaneIndex;
    public int endLaneIndex;

    public Lane(int startLaneIndex, int endLaneIndex)
    {
        this.startLaneIndex = startLaneIndex;
        this.endLaneIndex = endLaneIndex;
    }
}

public class GameInitData
{
    public Race race;

    public GameInitData(Race race)
    {
        this.race = race;
    }
}

public class Race
{
    public Track track;
    public Car[] cars;
    public RaceSession raceSession;

    public Race(Track track, Car[] cars, RaceSession raceSession)
    {
        this.track = track;
        this.cars = cars;
        this.raceSession = raceSession;
    }
}

public class Track
{
    public string id;
    public string name;
    public Piece[] pieces;
    public LaneData[] lanes;
    public StartingPoint startingPoint;

    public Track(string id, string name, Piece[] pieces, LaneData[] lanes, StartingPoint startingPoint)
    {
        this.id = id;
        this.name = name;
        this.pieces = pieces;
        this.lanes = lanes;
        this.startingPoint = startingPoint;
    }
}

public class Piece
{
    public double length;
    public bool SWITCH;
    public int radius;
    public double angle;

    public Piece(double length, bool sswitch, int radius, double angle)
    {
        this.length = length;
        this.SWITCH = sswitch;
        this.radius = radius;
        this.angle = angle;
    }
}

public class LaneData
{
    public int distanceFromCenter;
    public int index;

    public LaneData(int distanceFromCenter, int index)
    {
        this.distanceFromCenter = distanceFromCenter;
        this.index = index;
    }
}

public class StartingPoint
{
    public Position position;
    public double angle;

    public StartingPoint(Position position, double angle)
    {
        this.position = position;
        this.angle = angle;
    }
}

public class Position
{
    public double x;
    public double y;

    public Position(double x, double y)
    {
        this.x = x;
        this.y = y;
    }
}

public class Car
{
    public Id id;
    public Dimensions dimensions;

    public Car(Id id, Dimensions dimensions)
    {
        this.id = id;
        this.dimensions = dimensions;
    }
}

public class Dimensions
{
    public double length;
    public double width;
    public double guideFlagPosition;

    public Dimensions(double length, double width, double guideFlagPosition)
    {
        this.length = length;
        this.width = width;
        this.guideFlagPosition = guideFlagPosition;
    }
}

public class RaceSession
{
    public int laps;
    public int maxLapTimeMs;
    public bool quickRace;

    public RaceSession(int laps, int maxLapTimeMs, bool quickRace)
    {
        this.laps = laps;
        this.maxLapTimeMs = maxLapTimeMs;
        this.quickRace = quickRace;
    }
}

public class TurboData
{
    public double turboDurationMilliseconds;
    public int turboDurationTicks;
    public double turboFactor;
}