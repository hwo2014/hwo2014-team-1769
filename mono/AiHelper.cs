﻿public class OptimumConstantThrottle
{
    public const double Keimola = 0.7;//0.67;
    public const double Germany = 0.4;//0.48;

    public static double Get(string trackName, int currentIndex = -1)
    {
        //hardcode for testing
        //return 0.86;

        switch(trackName)
        {
            case "keimola":
                {
                    if (currentIndex <= 5)
                        return 0.65;//0.73;
                    else if (currentIndex <= 6)
                        return 0.65;//0.75;
                    else if (currentIndex <= 7)
                        return 1;
                    else if (currentIndex <= 8)
                        return 1;
                    else if (currentIndex <= 11)
                        return 1;
                    else if (currentIndex <= 17)
                        return 0.66;
                    else if (currentIndex <= 22)
                        return 0.65;//0.72;
                    else if (currentIndex <= 24)
                        return 1;
                    else if (currentIndex <= 27)
                        return 0.69;//0.75;
                    else if (currentIndex <= 30)
                        return 0.74;
                    else if (currentIndex <= 32)
                        return 0.65;
                    else if (currentIndex <= 34)
                        return 0.65;//0.73;
                    else
                        return Keimola;
                }
            case "germany":
                {
                    if (currentIndex <= 4)
                        return 0.45;
                    else if (currentIndex <= 8)
                        return 0.56;
                    else if (currentIndex <= 11)
                        return 0.55;
                    else if (currentIndex <= 13)
                        return 1;
                    else if (currentIndex <= 16)
                        return 1;
                    else if (currentIndex <= 18)
                        return 0.8;
                    else
                        return Germany;
                }
            case "usa":
                {
                    if (currentIndex <= 5)
                        return 0.92;
                    else if (currentIndex <= 11)
                        return 0.92;
                    else if (currentIndex <= 21)
                        return 0.83;
                    else if (currentIndex <= 27)
                        return 0.92;
                    else
                        return 0.85;
                }
            default : return 0.40;//safety net
        }
    }
}