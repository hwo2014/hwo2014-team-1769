﻿
using System;
using System.Collections.Generic;

public enum CarStatus
{
    OK,
    Crash
}

public class BotAi
{
    private CarPositionData[] _cars;
    private double _lastAngle;
    private GameInitData _gameInitData;
    private Piece[] _trackPieces;
    private double _lastInPieceDistance;
    private int _lastPieceIndex;
    private string _colorOfMyCar;
    private CarPositionData _myCar;
    private int _myCarPieceIndex = -1;
    private double _lastSpeed;
    private CarStatus _carStatus;
    private string _trackName;
    private double _lastAngularSpeed;
    private bool _turboAvailable;
    private TurboData _turboData;
    private Direction _switchDirection;
    private Car _myCarSpecs;
    private double _lastCarAngleRadius;
    private List<Piece> _nextSimilarPieces = new List<Piece>();
    private double _nextSimilarPiecesDistance = 0.0;
    private List<Piece> _nextNonSimilarPieces = new List<Piece>();
    private double _nextNonSimilarPiecesDistance = 0.0;

    public BotAi()
    {
    }

    public double CalculateThrottle(int gameTick)
    {
        double speed = CalculateSpeed();

        var accelleration = speed - _lastSpeed;

        _lastSpeed = speed;

        //var scalarVelocity = Math.Tan(_myCar.angle * Math.PI / 180) * speed;
        var angularSpeed = _myCar.angle - _lastAngle;
        var angularAccelleration = angularSpeed - _lastAngularSpeed;
        _lastAngularSpeed = angularSpeed;
        _lastAngle = _myCar.angle;

        bool isBend = _trackPieces[_myCarPieceIndex].radius > 0;

        double throttle = 0.0;

        if (isBend)
        {
            //if (speed < OptimumConstantThrottle.Get(_trackName, _myCarPieceIndex) * 10 - 0.0)
            //    throttle = OptimumConstantThrottle.Get(_trackName, _myCarPieceIndex);
            if(CheckAngularSpeed(angularSpeed, speed))
            {
                if (CheckSpeed(speed))
                {
                    throttle = GetThrottle(_trackPieces[_myCarPieceIndex].radius);
                }
            }
        }
        else
        {
            if (CheckSpeed(speed))
                throttle = 1;
        }

        //hardcode for testing
        //if(_myCar.piecePosition.lap == 0)
        //{
        //    throttle = 0.4;
        //}

        double carAngleRadius = _myCar.angle * Math.PI / 180 * (_myCarSpecs.dimensions.length - _myCarSpecs.dimensions.guideFlagPosition);

        int laneRadius;
        if (_gameInitData.race.track.pieces[_myCar.piecePosition.pieceIndex].angle > 0)
            laneRadius = _gameInitData.race.track.pieces[_myCar.piecePosition.pieceIndex].radius - _gameInitData.race.track.lanes[_myCar.piecePosition.lane.startLaneIndex].distanceFromCenter;
        else
            laneRadius = _gameInitData.race.track.pieces[_myCar.piecePosition.pieceIndex].radius + _gameInitData.race.track.lanes[_myCar.piecePosition.lane.startLaneIndex].distanceFromCenter;

        if (_carStatus == CarStatus.OK)
            Console.WriteLine("#{1, 2}, TA: {5,6:N2}, R: {2, 3}, LR: {10, 3}, S: {0:N2}, A: {3,5:N2}, CA: {4,6:N2}, CAS: {7,6:N2}, CAR: {8,6:N2}, CARS: {9,6:N2}, T: {6,5:N2}"
                , speed
                , _myCar.piecePosition.pieceIndex
                , _gameInitData.race.track.pieces[_myCar.piecePosition.pieceIndex].radius
                , accelleration
                , _myCar.angle
                , _gameInitData.race.track.pieces[_myCar.piecePosition.pieceIndex].angle
                , throttle
                , angularSpeed
                , carAngleRadius
                , carAngleRadius - _lastCarAngleRadius
                , laneRadius
                );

        _lastCarAngleRadius = carAngleRadius;

        return throttle;
    }

    private double GetThrottle(int radius)
    {
        switch(radius)
        {
            case 0:
                return 1.0;

            case 50:
                return 0.5;

            case 100:
                return 0.7;

            case 200:
                return 1.0;

            default:
                return (double)radius / 200;
        }
    }

    private bool CheckSpeed(double speed)
    {
        double currentPieceLength = 
            _trackPieces[_myCarPieceIndex].radius == 0 ? 
            _trackPieces[_myCarPieceIndex].length : 
            CalculateArcLength(_myCar.piecePosition.lane.startLaneIndex, _trackPieces[_myCarPieceIndex]);

        double distanceToNonSimilarPieces = currentPieceLength - _myCar.piecePosition.inPieceDistance + _nextSimilarPiecesDistance;

        double decelerationRate = speed * 0.02;//0.16;

        double nextSpeed = speed - decelerationRate;

        while (nextSpeed >= 
            //OptimumConstantThrottle.Get(_trackName, _myCarPieceIndex) * 10
            GetThrottle(_nextNonSimilarPieces[0].radius) * 10
            )
        {
            distanceToNonSimilarPieces -= nextSpeed;
            nextSpeed -= decelerationRate;
        }

        return distanceToNonSimilarPieces >= 0;        
    }

    private const double AngularLimit = 30.0;

    public bool CheckAngularSpeed(double angularSpeed, double carSpeed)
    {
        if (carSpeed == 0)
            return true;

        bool isCurrentBendPositive = _trackPieces[_myCarPieceIndex].angle > 0;

        bool analyzedPieceIsSimilar = true;
        int nextPieceIndex = GetIndex(_myCarPieceIndex + 1);

        var similarBendPieces = new List<Piece>();

        similarBendPieces.Add(_trackPieces[_myCarPieceIndex]);

        while (analyzedPieceIsSimilar)
        {
            analyzedPieceIsSimilar = 
                _trackPieces[nextPieceIndex].radius > 0
                && isCurrentBendPositive == _trackPieces[nextPieceIndex].angle > 0;

            if (analyzedPieceIsSimilar)
                similarBendPieces.Add(_trackPieces[nextPieceIndex]);

            nextPieceIndex = GetIndex(nextPieceIndex + 1);
        }

        var currentCarAngle = _myCar.angle;
        var currentCarDistance = _myCar.piecePosition.inPieceDistance;
        double totalArcLength = 0.0;

        foreach(var bendPiece in similarBendPieces)
        {
            totalArcLength += CalculateArcLength(_myCar.piecePosition.lane.startLaneIndex, bendPiece);
        }

        double leftOverBendDistance = totalArcLength - currentCarDistance;

        double projectedAngle = currentCarAngle + (leftOverBendDistance / carSpeed * angularSpeed);

        return Math.Abs(projectedAngle) <= AngularLimit;
    }

    private void AnalyzeNextPieces()
    {
        _nextSimilarPieces.Clear();
        _nextSimilarPiecesDistance = 0.0;
        _nextNonSimilarPieces.Clear();

        int currentRadius = _trackPieces[_myCarPieceIndex].radius;

        bool analyzedPieceIsSimilar = true;
        int nextPieceIndex = GetIndex(_myCarPieceIndex + 1);

        while (analyzedPieceIsSimilar)
        {
            analyzedPieceIsSimilar = _trackPieces[nextPieceIndex].radius == currentRadius;

            if (analyzedPieceIsSimilar)
            {
                _nextSimilarPieces.Add(_trackPieces[nextPieceIndex]);
                _nextSimilarPiecesDistance +=
                    _trackPieces[nextPieceIndex].radius == 0 ?
                    _trackPieces[nextPieceIndex].length
                    : CalculateArcLength(_myCar.piecePosition.lane.startLaneIndex, _trackPieces[nextPieceIndex]);

                nextPieceIndex = GetIndex(nextPieceIndex + 1);
            }
        }

        analyzedPieceIsSimilar = true;
        currentRadius = _trackPieces[nextPieceIndex].radius;

        while (analyzedPieceIsSimilar)
        {
            analyzedPieceIsSimilar = _trackPieces[nextPieceIndex].radius == currentRadius;

            if (analyzedPieceIsSimilar)
            {
                _nextNonSimilarPieces.Add(_trackPieces[nextPieceIndex]);
                _nextNonSimilarPiecesDistance +=
                    _trackPieces[nextPieceIndex].radius == 0 ?
                    _trackPieces[nextPieceIndex].length
                    : CalculateArcLength(_myCar.piecePosition.lane.startLaneIndex, _trackPieces[nextPieceIndex]);

                nextPieceIndex = GetIndex(nextPieceIndex + 1);
            }
        }
    }

    private double CalculateSpeed()
    {
        double speed;

        if (_lastPieceIndex != _myCar.piecePosition.pieceIndex)
        {
            if (_trackPieces[_lastPieceIndex].length > 0)
                speed = _trackPieces[_lastPieceIndex].length - _lastInPieceDistance + _myCar.piecePosition.inPieceDistance;
            else
            {
                double arcLength = CalculateArcLength(_myCar.piecePosition.lane.startLaneIndex, _trackPieces[_lastPieceIndex]);

                speed = arcLength - _lastInPieceDistance + _myCar.piecePosition.inPieceDistance;
            }
        }
        else
            speed = _myCar.piecePosition.inPieceDistance - _lastInPieceDistance;

        _lastPieceIndex = _myCar.piecePosition.pieceIndex;
        _lastInPieceDistance = _myCar.piecePosition.inPieceDistance;

        return speed;
    }    

    public void SetColorOfMyCar(string colorOfMyCar)
    {
        _colorOfMyCar = colorOfMyCar;
    }

    public void SetGameInitData(GameInitData gameInitData)
    {
        _gameInitData = gameInitData;
        _trackPieces = gameInitData.race.track.pieces;
        _trackName = gameInitData.race.track.id;

        foreach (var car in gameInitData.race.cars)
        {
            if (car.id.color == _colorOfMyCar)
            {
                _myCarSpecs = car;
                break;
            }
        }

        Console.WriteLine("My car length: {0}, width: {1}, guide flag position: {2}", _myCarSpecs.dimensions.length, _myCarSpecs.dimensions.width, _myCarSpecs.dimensions.guideFlagPosition);

        foreach (var piece in _trackPieces)
        {
            Console.WriteLine("L: {0,3:N0}, S: {1,5}, R: {2,3:N0}, A: {3,6:N2}", piece.length, piece.SWITCH, piece.radius, piece.angle);
        }
    }

    public void SetCarPositionData(CarPositionData[] cars)
    {
        _cars = cars;

        foreach (var car in _cars)
        {
            if (car.id.color == _colorOfMyCar)
            {
                _myCar = car;
                break;
            }
        }

        if (_myCarPieceIndex != _myCar.piecePosition.pieceIndex)
        {
            _myCarPieceIndex = _myCar.piecePosition.pieceIndex;
            AnalyzeNextPieces();
            AnalyzeSwitchDirection();
        }
    }

    public void SetCarStatus(CarStatus status)
    {
        _carStatus = status;
    }

    public void SetTurboAvailable(TurboData turboData)
    {
        _turboAvailable = true;
        _turboData = turboData;
    }

    public bool CanRunTurbo()
    {
        if (!_turboAvailable)
            return false;

        bool isCurrentPieceLine = _trackPieces[_myCar.piecePosition.pieceIndex].radius == 0;

        double totalLinePieceDistance = _trackPieces[_myCarPieceIndex].length + _nextSimilarPiecesDistance;

        bool indexCorrect =
            (totalLinePieceDistance >= (_turboData.turboDurationTicks * 10 * (_turboData.turboFactor / 3)))
            && isCurrentPieceLine
            && _nextNonSimilarPieces[0].radius > 50
            ;

        if(indexCorrect)
        {
            _turboAvailable = false;
            return true;
        }
        else
            return false;
    }

    public Direction GetSwitchDirection()
    {
        return _switchDirection;
    }

    private void AnalyzeSwitchDirection()
    {
        int immediateNextPiece = GetIndex(_myCarPieceIndex + 1);

        if (_trackPieces[immediateNextPiece].SWITCH)
        {
            var bendPiecesUntilNextSwitchPiece = new List<Piece>();

            int nextPieceIndex = GetIndex(immediateNextPiece + 1);
            bool analyzedPieceIsSwitch = false;

            while(!analyzedPieceIsSwitch)
            {
                if (_trackPieces[nextPieceIndex].SWITCH)
                    analyzedPieceIsSwitch = true;

                if (_trackPieces[nextPieceIndex].radius > 0)
                    bendPiecesUntilNextSwitchPiece.Add(_trackPieces[nextPieceIndex]);

                nextPieceIndex = GetIndex(nextPieceIndex + 1);
            }

            int currentLaneIndex = _myCar.piecePosition.lane.startLaneIndex;

            int numberOfLanes = _gameInitData.race.track.lanes.Length;

            int betterLaneIndex = currentLaneIndex;
            double betterLaneBendDistanceSum = CalculateTotalArcLength(bendPiecesUntilNextSwitchPiece, currentLaneIndex);

            int nextLaneIndex = currentLaneIndex + 1;
            if (nextLaneIndex < numberOfLanes)
            {
                double thisLaneBendDistanceSum = CalculateTotalArcLength(bendPiecesUntilNextSwitchPiece, nextLaneIndex);
                if(thisLaneBendDistanceSum < betterLaneBendDistanceSum)
                {
                    betterLaneIndex = nextLaneIndex;
                    betterLaneBendDistanceSum = thisLaneBendDistanceSum;
                }
            }

            int previousLaneIndex = currentLaneIndex - 1;
            if (previousLaneIndex >= 0)
            {
                double thisLaneBendDistanceSum = CalculateTotalArcLength(bendPiecesUntilNextSwitchPiece, previousLaneIndex);
                if (thisLaneBendDistanceSum < betterLaneBendDistanceSum)
                {
                    betterLaneIndex = previousLaneIndex;
                }
            }

            if(betterLaneIndex != currentLaneIndex)
            {
                var lanes = _gameInitData.race.track.lanes;
                if(lanes[betterLaneIndex].distanceFromCenter > lanes[currentLaneIndex].distanceFromCenter)
                {
                    _switchDirection = Direction.Right;
                }
                else
                {
                    _switchDirection = Direction.Left;
                }
            }
            else
            {
                _switchDirection = Direction.None;
            }
        }
    }

    private double CalculateTotalArcLength(List<Piece> bendPiecesUntilNextSwitchPiece, int laneIndex)
    {
        double totalArcLength = 0.0;

        foreach (var bendPiece in bendPiecesUntilNextSwitchPiece)
        {
            totalArcLength += CalculateArcLength(laneIndex, bendPiece);
        }

        return totalArcLength;
    }

    private double CalculateArcLength(int laneIndex, Piece bendPiece)
    {
        int laneRadius;
        if (bendPiece.angle > 0)
            laneRadius = bendPiece.radius - _gameInitData.race.track.lanes[laneIndex].distanceFromCenter;
        else
            laneRadius = bendPiece.radius + _gameInitData.race.track.lanes[laneIndex].distanceFromCenter;

        double arcLength = Math.Abs(bendPiece.angle) * Math.PI / 180 * laneRadius;
        return arcLength;
    }

    private int GetIndex(int index)
    {
        return index >= _trackPieces.Length ? index - _trackPieces.Length : index;
    }

    public void ResetSwitchDirection()
    {
        _switchDirection = Direction.None;
    }

    private double CalculateOptimumSpeed()
    {
        var currentCarAngle = _myCar.angle;
        var immediateNextBendPiece = _nextNonSimilarPieces[0];

        //positive angle gives positive slip angle
        var bendPieceAngle = immediateNextBendPiece.angle;

        //bigger radius means smaller angular velocity
        var bendPieceRadius = immediateNextBendPiece.radius;

        return 0.0;
    }
}
