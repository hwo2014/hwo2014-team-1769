using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;

public class Bot {
	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

            //var joinRace = new JoinRace(botName, botKey, "keimola");
            //var joinRace = new JoinRace(botName, botKey, "germany");
            string track =
                //"keimola";
                //"germany";
                //"usa";
                //"france";
                "elaeintarha";
                //"imola";
                //"england";
                //"suzuka";

            var joinRace = new JoinRace(botName, botKey, track);

            //quickrace
            new Bot(reader, writer, new Join(botName, botKey));

            //custom race
            //new Bot(reader, writer, joinRace);
		}
	}

	private StreamWriter _writer;
    private BotAi _botAi;

    Bot(StreamReader reader, StreamWriter writer, SendMsg join)
    {
        _botAi = new BotAi();

		this._writer = writer;
		string line;

		send(join);

		while((line = reader.ReadLine()) != null) {
			var msg = JsonConvert.DeserializeObject<MsgWrapperEx>(line);
            HandleMsg(msg);
		}
	}

    private void HandleMsg(MsgWrapperEx msg)
    {
        switch (msg.msgType)
        {
            case "carPositions":
                var cars = JsonConvert.DeserializeObject<CarPositionData[]>(msg.data.ToString());
                _botAi.SetCarPositionData(cars);

                if (_botAi.CanRunTurbo())
                {
                    Console.WriteLine("GO!!!");
                    send(new Turbo());
                }

                var throttle = _botAi.CalculateThrottle(msg.gameTick);
                var switchDirection = _botAi.GetSwitchDirection();
                if (switchDirection != Direction.None)
                {
                    Console.WriteLine("Switch Direction: {0}", switchDirection);
                    send(new SwitchLane(switchDirection));
                    _botAi.ResetSwitchDirection();
                }
                else
                {
                    send(new Throttle(throttle, msg.gameTick));
                }
                break;
            
            case "join":
                Console.WriteLine("Joined");
                BotId joinwrapper = JsonConvert.DeserializeObject<BotId>(msg.data.ToString());
                Console.WriteLine("Name: {0}, Key: {1}", joinwrapper.name, joinwrapper.key);
                send(new Ping());
                break;
            
            case "yourCar":
                var myCar = JsonConvert.DeserializeObject<Id>(msg.data.ToString());
                Console.WriteLine("My Car Name: {0}, Color: {1}", myCar.name, myCar.color);
                _botAi.SetColorOfMyCar(myCar.color);
                break;

            case "gameInit":
                Console.WriteLine("Race init");
                var gameInitData = JsonConvert.DeserializeObject<GameInitData>(msg.data.ToString());
                _botAi.SetGameInitData(gameInitData);
                Console.WriteLine("Track ID: {0}, Track Name: {1}", gameInitData.race.track.id, gameInitData.race.track.name);
                Console.WriteLine("Laps: {0}, max lap time: {1} ms", gameInitData.race.raceSession.laps, gameInitData.race.raceSession.maxLapTimeMs);
                send(new Ping());
                break;
            
            case "gameEnd":
                Console.WriteLine("Race ended");
                send(new Ping());
                break;
            
            case "gameStart":
                Console.WriteLine("Race starts");
                send(new Ping());
                break;
            
            case "crash":
                Console.WriteLine("Damn it! Crash!");
                _botAi.SetCarStatus(CarStatus.Crash);
                break;
            
            case "spawn":
                Console.WriteLine("Alright, restored!");
                _botAi.SetCarStatus(CarStatus.OK);
                break;

            case "turboAvailable":
                var turboData = JsonConvert.DeserializeObject<TurboData>(msg.data.ToString());
                Console.WriteLine("duration: {0}, tick duration: {1}, factor: {2}", turboData.turboDurationMilliseconds, turboData.turboDurationTicks, turboData.turboFactor);
                _botAi.SetTurboAvailable(turboData);
                break;

            case "error":
                Console.WriteLine("Error! {0}", msg.data);
                break;
            
            default:
                send(new Ping());
                break;
        }
    }

	private void send(SendMsg msg) {
		_writer.WriteLine(msg.ToJson());
	}
}